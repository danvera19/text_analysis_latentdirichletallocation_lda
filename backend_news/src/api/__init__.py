from flask import Flask, request, Response
from pymongo import MongoClient
from newspaper import Article
from datetime import datetime

import json
import collections
import logging
import model
import pandas as pd

app = Flask(__name__)
logging.basicConfig(level=logging.INFO)

mongo = MongoClient("mongodb://db:27017/")
db = mongo["newsdb"]
news_data = db["news"]

############################

news_data.delete_many({})
pre = json.load(open("worldnews.json", "r"))
pre = [dict(t) for t in {tuple(d.items()) for d in pre}]
pre = [{"title": d["title"], "url": d["url"], "content": d["content"], "topics": False} for d in pre]
news_data.insert_many(pre)

############################

def update_model():
    model.LDA(model.pass_content(app, news_data))

def update_recommends():
    model.get_df_from_db(news_data, app)

def update_all():
    update_model()
    no_topics = news_data.find({"topics": False})
    for nt in no_topics:
        content = nt.get("content")
        _, df = model.grp_nuevo_texto(content, False)
        app.logger.info(df)
        for be, pr in zip(df["Belong"].astype(str).tolist(), df["Proba"].tolist()):
            news_data.update({"url": nt.get("url")}, {"$set": {be: pr}})
        news_data.update({"url": nt.get("url")}, {"$set": {"topics": True}})
    update_recommends()

update_all()

############################


def extract_content(url):
    try:
        article = Article(url)
        article.download()
        article.parse()
        return article.text
    except:
        return ""


@app.route("/topics", methods=["GET", "POST"])
def topics():
    if request.method == "GET":
        topics = []
        for idx in range(len(model.final_list)):
            tupla = model.final_list[idx]
            xy = [(float(pr), to.replace('"', "")) for (pr, to) in [x.strip().split("*") for x in tupla[1].split("+")]]
            y = [y for x, y in xy]
            topics_string = " ".join(y)
            topics.append(topics_string)

        app.logger.info(topics)
        return json.dumps({
            "topics": topics
        })

    if request.method == "POST":
        data = request.get_json()
        topic = int(data["topic"])
        bar = model.plot_topic(topic, app)

        tpc = str(data["topic"])

        topic_news = list(news_data.find({tpc: {"$exists": True}}, {"content": 0, "topics": 0}).sort(tpc, -1))[:10]
        title_news = [tn["title"] for tn in topic_news]
        url_news = [tn["url"] for tn in topic_news]
        proba_news = [tn[tpc] for tn in topic_news]

        return json.dumps({
            "image": [bar],
            "recoms": title_news,
            "sims": proba_news,
            "urls": url_news
        })




@app.route("/update", methods=["GET"])
def update():
    if request.method == "GET":
        app.logger.info("TIME TO UPDATE THE MODEL")

        update_all()

        all_data = list(news_data.find({}, {"_id": 0}))
        now_date = datetime.now().strftime("_%y%m%d_%H%M%S")
        all_file = "worldnews{}.json".format(now_date)
        with open(all_file, "w") as file:
            json.dump(all_data, file)
            app.logger.info("DATABASE SAVED AS JSON TO: '{}'".format(all_file))



        return Response(status = 200)



@app.route("/lda", methods=["POST"])
def lda():
    if request.method == "POST":
        data = request.get_json()
        # title = data["title"]
        url = data["url"]
        content = extract_content(url)
        result_image, result_df = model.grp_nuevo_texto(content)


        topics = dict(zip(result_df["Belong"].astype(str).tolist(), result_df["Proba"].tolist()))
        topics["_id"] = "temp"

        topics_df = pd.DataFrame([topics.values()], columns = topics.keys())
        topics_df = topics_df.set_index("_id").T
        app.logger.info(topics_df)
        recoms = model.busca_simi(topics_df, topics["_id"], app)
        recoms = recoms.to_dict()[0]
        # app.logger.info(recoms)

        recoms_ids = list(recoms.keys())
        recoms_sims = [round(r*100, 2) for r in list(recoms.values())]
        # app.logger.info(recoms_ids)
        # recoms_ids = [str(r) for r in recoms.keys()]

        recoms_news = list(news_data.find({"_id": {"$in": recoms_ids}}))
        # app.logger.info(recoms_news)
        recoms_titles = [r["title"] for r in recoms_news]
        recoms_urls = [r["url"] for r in recoms_news]
        # app.logger.info(recoms_titles)

        bars = []
        for idx in result_df["Belong"].astype(int).tolist():
            bar = model.plot_topic(idx, app)
            bars.append(bar)


        return json.dumps({
            # "title": title,
            "url": url,
            "result": str(result_df),
            "image": result_image,
            "recoms": recoms_titles,
            "urls": recoms_urls,
            "sims": recoms_sims,
            "topics": bars
        })
