import string
import nltk
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import stopwords, wordnet
nltk.download('stopwords')
nltk.download('wordnet')

punctuation = set(string.punctuation)
stoplist = set(stopwords.words('english') + ["said"])

lemma = WordNetLemmatizer()

def remove_punctuation(text):
    return ''.join([char for char in text if char not in punctuation])


def remove_numbers(text):
    return ''.join([char for char in text if not char.isdigit()])


def remove_stopwords(text):
    return ' '.join([word for word in text.split() if word not in stoplist])


def remove_single_chars(text):
    return ' '.join([word for word in text.split() if len(word) > 1])


def lemmatize(text):
    return ' '.join([lemma.lemmatize(word) for word in text.split()])


def clean_text(text):
    text = text.replace('\n', '').lower()
    text = remove_punctuation(text)
    text = remove_numbers(text)
    text = remove_stopwords(text)
    text = lemmatize(text)
    text = remove_single_chars(text)
    return text
