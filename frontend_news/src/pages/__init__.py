from flask import Flask, jsonify, render_template, request

# import pandas as pd
# import numpy as np
# import pymongo
import json
import random
import requests

app = Flask(__name__, template_folder="templates")
# app = Flask(__name__, template_folder="/app/src/api/templates", static_folder="/app/src/api/static")

@app.route("/", methods=["GET", "POST"])
def index():
    if request.method == "GET":
        todayreq = requests.get("http://scraper:5000/today")
        posts = json.loads(todayreq.text)["posts"]

        topicsreq = requests.get("http://backend:5000/topics")
        topics = json.loads(topicsreq.text)["topics"]

        return render_template(
            "index.html",
            news=zip([p["title"] for p in posts], [p["url"] for p in posts]),
            newsarr=json.dumps(posts),
            topics=topics
        )


@app.route("/lda", methods=["POST"])
def lda():
    if request.method == "POST":
        data = request.get_json()

        headers = {"Content-type": "application/json", "Accept": "text/plain"}
        r = requests.post("http://backend:5000/lda", data=json.dumps({"url": data["url"]}), headers=headers)
        result = json.loads(r.text)

        html = render_template(
            "lda.html",
            # image = random.sample([0, 1, 2, 3], 1)[0],
            title = str(data["title"]),
            url = str(result["url"]),
            # result = str(result["result"]),
            image = result["image"]
        )

        recoms = render_template(
            "recommendations.html",
            recoms=zip(result["recoms"], result["urls"], result["sims"])
        )

        topics = render_template(
            "topics_carousel.html",
            topics=result["topics"]
        )

        return json.dumps({
            "html": html,
            "recoms": recoms,
            "topics": topics
        })


@app.route("/topics", methods=["POST"])
def topics():
    if request.method == "POST":
        data = request.get_json()

        headers = {"Content-type": "application/json", "Accept": "text/plain"}
        r = requests.post("http://backend:5000/topics", data=json.dumps({"topic": data["topic"]}), headers=headers)
        result = json.loads(r.text)

        topics = render_template(
            "topics_carousel.html",
            topics=result["image"]
        )

        recoms = render_template(
            "recommendations.html",
            recoms=zip(result["recoms"], result["urls"], [round(float(s)*100, 2) for s in result["sims"]])
        )

        return json.dumps({
            "topics": topics,
            "recoms": recoms
        })
