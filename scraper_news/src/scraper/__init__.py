from flask import Flask, request, Response
from newspaper import Article
from apscheduler.schedulers.background import BackgroundScheduler
from pymongo import MongoClient

import json
import praw
import logging
import atexit
import requests

from multiprocessing.dummy import Pool


app = Flask(__name__)
logging.basicConfig(level=logging.INFO)

client_id = "LsHNl431GwoQLw"
client_secret = "5R-ENpk1ytNXYGO53tTWgOlmn1o"
user_agent = "RedditWebScraping"
reddit = praw.Reddit(client_id=client_id, client_secret=client_secret, user_agent=user_agent)
worldnews = reddit.subreddit("worldnews")

mongo = MongoClient("mongodb://db:27017/")
db = mongo["newsdb"]
news_data = db["news"]


def extract_content(url):
    try:
        article = Article(url)
        article.download()
        article.parse()
        return article.text
    except:
        return ""



def get_post(post):
    rep_news = news_data.find({"url": post.url})
    if not len(list(rep_news)) > 0:
        content = extract_content(post.url)
        if not content == "":
            return {
                "title": post.title,
                "url": post.url,
                "content": content,
                "topics": False
            }
        else:
            return None
    else:
        return None


def update(limit=50, by="rising"):
    # posts = []

    if by=="rising":
        worldnews_gen = worldnews.rising(limit=limit)
    elif by=="hot":
        worldnews_gen = worldnews.hot(limit=limit)

    pool = Pool(50)
    posts = pool.map(get_post, worldnews_gen)
    pool.close()
    pool.join()

    posts = [p for p in posts if p is not None]

    app.logger.info("NEW POSTS:" + str(len(posts)))
    if posts:
        news_data.insert_many(posts)
        try:
            requests.get("http://backend:5000/update")
        except:
            app.logger.info("Backend not available.")


# update(500, "hot")

scheduler = BackgroundScheduler()
scheduler.add_job(func=update, trigger="interval", seconds=300)
scheduler.start()
atexit.register(lambda: scheduler.shutdown())



@app.route("/today", methods=["GET"])
def today():
    if request.method == "GET":
        posts = []
        for post in worldnews.hot(limit=15):
            posts.append({
                "title": post.title,
                "url": post.url
            })

        return json.dumps({
            "posts": posts
        })
